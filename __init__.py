# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

bl_info = {
    "name": "Matrioshkate",
    "author": "Madman’s Nest",
    "version": (1, 0),
    "blender": (2, 80, 0),
    "location": "3DView Operator",
    "description": "Extrude and scale the selection down towards the center in a given number of steps",
    "warning": "",
    "category": "Mesh",
}

import bpy
from bpy.props import IntProperty

class Matrioshkate(bpy.types.Operator):
    """Extrude and scale the selection down towards the center in a given number of steps"""
    bl_idname = "object.matrioshkate"
    bl_label = "Matrioshkate"
    bl_options = {'REGISTER', 'UNDO'}
    
    steps: IntProperty(
        name = "Steps",
        description = "Number of extrude and scale steps",
        default = 3,
        min = 1
    )

    @classmethod
    def poll(cls, context):
        return context.active_object is not None

    def execute(self, context):
        for i in reversed(range(self.steps)):
            scaleStep = 1 - (1/float(i+1))
            bpy.ops.mesh.extrude_region_move(MESH_OT_extrude_region={"use_normal_flip":False, "use_dissolve_ortho_edges":False, "mirror":False})
            bpy.ops.transform.resize(value=(scaleStep, scaleStep, scaleStep), constraint_axis=(False, False, False))
        return {'FINISHED'}

def register():
    bpy.utils.register_class(Matrioshkate)

def unregister():
    bpy.utils.unregister_class(Matrioshkate)